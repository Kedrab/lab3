#include <stdio.h>
#include <signal.h>
#include <unistd.h>
#include <sys/mman.h>
#include <alchemy/task.h>
#include <alchemy/timer.h>

RT_TASK demo_task;

void demo(void *arg){//<−−−USE IT IN APP
RT_TASK *curtask;
RT_TASK_INFO curtaskinfo;

printf("Hello  World !\n");

curtask=rt_task_self();
rt_task_inquire(curtask, &curtaskinfo);

printf("Task name:%s\n", curtaskinfo.name);
}

int main(int argc, char*argv[]){
char str[10];
// Lock memory  :  avoid memory swapping for this program
mlockall (MCL_CURRENT|MCL_FUTURE) ;
printf("start  task\n") ;

rt_task_create(&demo_task, str, 0, 50, 0); // arguments: &task, name, stack size, priority, modes
rt_task_start(&demo_task, &demo, 0); //arguments: &task, task function, function argument

}