#include <stdio.h>
#include <signal.h>
#include <unistd.h>
#include <sys/mman.h>

#include <alchemy/task.h>
#include <alchemy/timer.h>
#include <alchemy/sem.h>

#include <rtdk.h>

#define ITER 10

RT_TASK demo_task1;
RT_TASK demo_task2;

void taskOne(void *arg)
{
    int i;
    for(i=0; i<ITER; i++)
    {
        rt_printf("This is taskOne. Right now global variable is %d", ++global);
    }
}

void taskTwo(void *arg)
{
    int i;
    for(i=0; i<ITER; i++)
    {
        rt_printf("This is taskTwo. Right now global variable is %d", --global);
    }
}


int main(int argc, char* argv[])
{
    rt_print_auto_init(1);

    mlockall(MCL_CURRENT|MCL_FUTURE);

    rt_task_create(&t1, "task1", 0, 1, 0);
    rt_task_create(&t2, "task2", 0, 1, 0);

    rt_task_start(&t1, &taskOne, 0);
    rt_task_start(&t2, &taskTwo, 0);

    return 0;
}